package com.pdh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdh.entity.User;

/**
 */
public interface UserMapper extends BaseMapper<User> {
}
